--
-- Create database
--

DROP DATABASE IF EXISTS `onetimeonly`;
CREATE DATABASE `onetimeonly`;
USE `onetimeonly`;

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `items` (
  `id` char(25) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `value` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_bin;

--
-- Create user
--
GRANT SELECT, INSERT, DELETE ON `onetimeonly`.`items` TO `onetimeonly`@`localhost` IDENTIFIED BY 'insecure';
FLUSH PRIVILEGES;