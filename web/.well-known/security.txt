# Security reports for https://transfer.pw/
Contact: mailto:info@transfer.pw
Encryption: https://keys.openpgp.org/search?q=info@transfer.pw
Preferred-Languages: de, en
