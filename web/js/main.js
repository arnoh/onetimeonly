/**
 * Encrypt the message in the form
 */
function encryptMessage() {
    var msgElement, msg, pwd, params, encMsgJson;

    msgElement = document.getElementById('message');
    msg = msgElement.value;

    params = {
        iter:   1500,   // iterations for PBKDF2
        cipher: "aes",  // symmetric cipher for encryption
        ks:     128,    // key length in bits
        mode:   "gcm",  // block mode of the symmetric cipher
        ts:     64      // tag length in bits
    };

    sjcl.random.addEntropy(msg, msg.length/2);
    params.iv = sjcl.random.randomWords(4, 0);      // generate the IV randomly for each message
    params.salt = sjcl.random.randomWords(2, 0);    // salt the password with a random number
    pwd = generatePassword();
    sessionStorage.setItem('password', pwd);        // store password in the browser only
    encMsgJson = sjcl.encrypt(pwd, msg, params);    // encrypt!
    msgElement.value = encMsgJson;                  // replace the message with its encryption
}

/**
 * Generate random password, 25 chars, 62 values/char -> max. ~148bit
 * @returns {string}
 */
function generatePassword() {
    var bits = sjcl.random.randomWords(10, 0);
    var b64 = sjcl.codec.base64.fromBits(bits);
    return b64.replace(/[+=\/]/g, '').substr(0, 25);
}

/**
 * Read password from browser memory and append to URL
 */
function addPasswordToUrl() {
    if (sessionStorage.getItem('password') === null) {
        $('#transferLink').replaceWith(transferTemplates.errorRecallingPassword);
        $('#copyButton').prop('disabled', true);
        return false;
    }
    else {
        $('#transferLink')
            .append('#' + sessionStorage.getItem('password'))
            .on('mousedown', function (evt) {
                selectText('transferLink');
                evt.preventDefault();
            });
        sessionStorage.removeItem('password');
        $('#copyButton').on('click', copyToClipboard);
        return true;
    }
}

/**
 * Fetch the encrypted message and decrypt it
 */
function loadAndDecryptMessage() {
    $.getJSON(location.pathname, {fetch: "js"})
        .done(function (data) {
            if (data.itemExists) {
                decryptMessage(data.item);
                if (!data.deleted) {
                    $('#deleteNotice').replaceWith(transferTemplates.errorDeletingMessage);
                }
            } else {
                $('#showMessage').replaceWith(transferTemplates.errorLoading);
            }
        })
        .fail(function () {
            $('#showMessage').replaceWith(transferTemplates.errorLoading);
        })
}

/**
 * Decrypt a message that was encrypted with SJCL
 * @param msgJson  Message to decrypt
 */
function decryptMessage(msgJson) {
    var pwd, plainMsg;

    try {
        pwd = location.hash.slice(1);
        plainMsg = sjcl.decrypt(pwd, msgJson);
        $('#msgContent').text(plainMsg);
    }
    catch (e) {
        $('#showMessage').replaceWith(transferTemplates.errorDecrypting);
    }
}

/**
 * Routines for collecting entropy from user interactions
 */

function collectEntropy(evt) {
    var t = evt.type;
    var val = Date.now() + t.charCodeAt(0)*123 + t.charCodeAt(t.length-1);
    if ("pageX" in evt) {
        val ^= (2000*evt.pageX + evt.pageY);
    }
    sjcl.random.addEntropy(val, 1);
}

function startCollectingEntropy() {
    $('body')
        .on('click', collectEntropy)
        .on('mouseup', collectEntropy)
        .on('mousedown', collectEntropy)
        .on('keypress', collectEntropy)
        .on('keydown', collectEntropy)
        .on('keyup', collectEntropy);
    sjcl.random.startCollectors();    // SJCL collectors track mouse movement
}

/**
 * Selecting text in an element
 * @param element
 */
function selectText(element) {
    var doc = document
        , text = doc.getElementById(element)
        , range, selection
        ;
    if (doc.body.createTextRange) { //ms
        range = doc.body.createTextRange();
        range.moveToElementText(text);
        range.select();
    } else if (window.getSelection) { //all others
        selection = window.getSelection();
        range = doc.createRange();
        range.selectNodeContents(text);
        selection.removeAllRanges();
        selection.addRange(range);
    }
}

/**
 * Copy text to clipboard
 */
function copyToClipboard() {
    selectText('transferLink');
    var successful;
    try {
        successful = document.execCommand('copy');
    } catch(err) {
        successful = false;
    }
    if (!successful) {
        $('#copyButton').prop('disabled', true).text('error');
    }
}

/**
 * Some error messages
 */
var transferTemplates = {
    errorLoading:
        '<div class="container">' +
        '<div class="alert alert-danger">' +
        '<h3>Unable to load message</h3>' +
        '<p>There was an error while retrieving your message. This is our fault. We are terribly sorry. Please try again.</p>' +
        '</div></div>',
    errorDeletingMessage:
        '<div class="container">' +
        '<div class="alert alert-danger">' +
        '<h3>Unable to delete message on server</h3>' +
        '<p>This is unexpected: we were unable to delete the message from the server.</p>' +
        '<p>Please copy (and/or remember) the message above. Then try to reload the page. If all goes well, ' +
        'then you should see the same message again <strong>and</strong> it will be deleted. ' +
        'If not, then please contact us.</p>' +
        '<p>If you see the "message does not exist" warning on reload, ' +
        'this means that the message was already safely deleted (despite this warning which you are reading now).</p>' +
        '</div></div>',
    errorDecrypting:
        '<div class="container">' +
        '<div class="alert alert-danger">' +
        '<h3>Unable to decrypt message</h3>' +
        '<p>This is embarrassing, we were unable to decrypt the message. ' +
        'One possible cause is a mistake in the URL.' +
        '<br>For security reasons, the message has already been deleted on the server. ' +
        'We are sorry.</p>' +
        '</div></div>',
    errorRecallingPassword:
        '<div class="container">' +
        '<div class="alert alert-danger">' +
        '<p><strong>Error:</strong> We were unable to recall the password in your browser.' +
        '<br>Either this was a mistake on our side, then please try submitting the message again.' +
        '<br>Or your browser does not support HTML5 sessionStorage.' +
        '<br>In this case, please try again with another browser.' +
        '</p></div></div>'
};
