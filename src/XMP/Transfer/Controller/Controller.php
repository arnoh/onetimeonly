<?php

namespace XMP\Transfer\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;

abstract class Controller
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var Environment
     */
    protected $twig;


    public function __construct(Request $request, Environment $twig)
    {
        $this->request = $request;
        $this->twig    = $twig;
    }

    public abstract function getResponse(): Response;
}