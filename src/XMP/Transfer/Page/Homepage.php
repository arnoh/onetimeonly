<?php
/**
 * Created by PhpStorm.
 * User: arno
 * Date: 07/06/14
 * Time: 11:44
 */

namespace XMP\Transfer\Page;

use RuntimeException;
use Symfony\Component\HttpFoundation\Response;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use XMP\Transfer\Controller\Controller;

class Homepage extends Controller
{
    /**
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function getResponse(): Response
    {
        $response = new Response();

        // Generate some random bytes for the client side
        $bytes = openssl_random_pseudo_bytes(512/8, $strong);
        if (!$strong) {
            throw new RuntimeException("Secure random source is broken!");
        }

        $template = $this->twig->load('homepage.twig');
        $response->setContent($template->render([
            'entropy' => base64_encode($bytes)
        ]));
        $response->setMaxAge(3600);
        return $response;
    }

}