<?php

namespace XMP\Transfer\Page;

use Symfony\Component\HttpFoundation\Response;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use XMP\Transfer\Controller\Controller;

class Faq extends Controller
{
    /**
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function getResponse(): Response
    {
        $response = new Response();
        $template = $this->twig->load('faq.twig');
        $response->setContent($template->render());
        $response->setMaxAge(86400);
        return $response;
    }

}