<?php
namespace XMP\Transfer\Page;

use Symfony\Component\HttpFoundation\Response;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use XMP\Transfer\Controller\Controller;
use XMP\Transfer\Model\ItemRepository;

class View extends Controller
{
    /**
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function getResponse(): Response
    {
        $id = substr($this->request->getPathInfo(), 1);
        $rp = new ItemRepository();
        $item = $rp->getById($id);
        $itemOk = $item !== false;
        $itemExists = $item !== null && $itemOk;
        $response = new Response();

        if ($this->request->getQueryString() !== "fetch=js") {
            $template = $this->twig->load('view.twig');
            $response->setContent($template->render([
                'itemOk' => $itemOk,
                'itemExists' => $itemExists
            ]));
        } else {
            $deleted = $rp->deleteById($id);
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode([
                'itemExists' => $itemExists,
                'deleted' => $deleted,
                'item' => $itemExists ? $item->value : ""
            ]));
        }

        $response->headers->set('Cache-Control', 'private, max-age=0, no-cache, no-store');
        return $response;
    }
}