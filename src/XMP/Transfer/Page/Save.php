<?php

namespace XMP\Transfer\Page;

use DateTime;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use XMP\Transfer\Controller\Controller;
use XMP\Transfer\Model\Item;
use XMP\Transfer\Model\ItemRepository;

class Save extends Controller
{
    /**
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function getResponse(): Response
    {
        $template   = $this->twig->load('saved.twig');
        $msg        = $this->request->get('message');
        $msgIsValid = $this->validateMsg($msg);

        if ($msgIsValid) {
            $item          = new Item();
            $id            = $item->generateId();
            $item->created = new DateTime();
            $item->value   = $msg;
            $rp            = new ItemRepository();
            $msgIsStored   = $rp->store($item);
        } else {
            $msgIsStored = false;
            $id          = '';
        }

        $result = ['msgIsValid'  => $msgIsValid,
                   'msgIsStored' => $msgIsStored,
                   'id'          => $id
        ];

        if ($this->request->get('format') == 'json') {
            $response = new JsonResponse();
            $response->setData($result);
        } else {
            $response = new Response();
            $response->setContent($template->render($result));
        }

        $response->headers->set('Cache-Control', 'private, max-age=0, no-cache, no-store');
        return $response;
    }

    /**
     * Verify that JSON is correct, no too long, and contains all necessary characters
     * @param $msg string JSON encoding of message
     * @return bool
     */
    private function validateMsg(string $msg): bool
    {
        if (strlen($msg) > 3750)
            return false;
        if (preg_match('#[^ a-z0-9+/=":,\n{}]#i', $msg))
            return false;
        $data = json_decode($msg, true, 2);
        if (!is_array($data))
            return false;
        $validKeys = ['v'    => 0, 'iv' => 0, 'salt' => 0, 'ks' => 0, 'ts' => 0, 'iter' => 0,
                      'mode' => 0, 'adata' => 0, 'ct' => 0, 'cipher' => 0];
        foreach (array_keys($data) as $key) {
            if (!array_key_exists($key, $validKeys))
                return false;
            $validKeys[$key]++;
        }
        foreach ($validKeys as $cnt) {
            if ($cnt != 1)
                return false;
        }
        return true;
    }
}
